﻿*** a9ab0d60c4bb64a6ae5b2b0bde5e0b25
## 这个模型是什么？

如果您生活在夏暖冬冷的气候中，那么您可能对美丽的秋天现象很熟悉，树叶在枯死和从树上脱落之前会变色。该模型模拟了树叶改变颜色和掉落的方式，使探索和了解这一美丽的年度奇观成为可能。

## 这个模型是怎么运行的

叶子为什么以及如何改变颜色和秋天是非常复杂的，并且与阳光、热量和雨水的结合有关。 （叶子即使没有变色也会被强风吹走，所以风也有作用。）

我们在每片叶子上看到的颜色源于每片叶子中产生和储存的天然物质的存在。三种物质有助于叶子的颜色：

- 绿色来自叶绿素（或称为叶绿素的一组相关物质），它将阳光和水转化为糖。当暴露在过多的阳光下和温度较低时，叶绿素分子会被破坏并且不会得到补充。因此，寒冷晴朗的秋季使叶绿素的整体浓度降低。在阳光下（只要不是太多！）和有水时，总叶绿素浓度会再次上升。

- 黄色来自一种叫做胡萝卜素的物质。胡萝卜素分子有助于为胡萝卜和红薯着色。胡萝卜素的浓度在叶子的整个生命周期中保持不变。然而，黄色经常被叶绿素的绿色所掩盖。含有更多叶绿素的叶子（夏季典型）将完全是绿色的，尽管在绿色后面掩盖了强烈的黄色色调。然而，随着叶绿素死亡，胡萝卜素的存在变得明显，导致叶子变黄。

- 红色来自一种叫做花青素的物质。花青素分子是在叶子中存在高糖浓度和水浓度的情况下产生的。 （糖的浓度越高，产生的花青素就越多。）当寒冷的天气导致树木关闭与树木其余部分的水循环时，糖的浓度会增加；任何被困在叶子中的水和糖都会被转化为花青素。

模型中时钟的每个周期都包含两个阶段：（1）天气（雨、风、太阳）影响叶子，酌情添加或去除糖、水或叶绿素，以及（2）叶子对其环境做出反应，酌情添加花青素，并改变颜色以反映改良的环境。

水不会直接进入每片叶子，而是被树根吸收，然后从树根向上拉到树枝和树叶中。在这个模型中，假设整个地面都包含树根，因此所有雨滴一旦到达地面就会流向树干。类似地，所有雨滴都沿着树干向上移动，然后沿着树枝（模型中未显示）到达树叶。树叶从附近的雨滴中收集水分。当雨滴中没有更多的水时，它们就会消失。

模型中的叶子有一个“附着”属性，模型用它来指示叶子对树的附着强度。执着随水而升，随风雨而减。 （在刮风的日子里，即使叶子完全是绿色的，它们也可能会被吹走。）

由于 NetLogo 颜色空间不包括所有可能的颜色，该模型使用阈值算法来确定叶子的颜色，作为真实颜色的近似值。当叶绿素超过 50% 时，叶子是绿色的。在此之下，叶子被赋予黄色、红色或橙色，这取决于是否有大部分胡萝卜素、花青素或两者兼而有之。

请注意，叶子颜色的强度随叶绿素、胡萝卜素和/或花青素的水平而变化。所以一棵富含叶绿素的树会有深绿色的叶子，而只有一点叶绿素的树会有浅绿色的叶子。

## 如何这个模型怎么用

要使用该模型，请按 SETUP。这将创建树干和树枝，还将创建树的叶子，它们是该模型中的主要主体。按 GO 使系统启动。

现在是有趣和/或棘手的部分，即调整视图下的滑块，以便天气产生您想要探索或研究的条件。如果由于缺水（您可以在“叶子平均值”图表中进行监控），叶子似乎正在失去叶绿素，您可以通过调整 RAIN-INTENSITY滑块来使其下雨。要使风吹，请调整 WIND-FACTOR滑块。

太阳的强度（就阳光强度而言）通过 SUN-INTENSITY滑块设置。如上所述，叶子需要阳光来产生叶绿素和糖——但过多的阳光可能会开始破坏这些化学物质。

最后，您可以更改温度滑块。如果温度太低，叶绿素分子将被破坏，从而启动花青素的产生。

掌握了模型的基础知识后，您可能需要考虑设置左上角的两个滑块，START-SUGAR-MEAN 和 START-SUGAR-STDDEV，它们会影响糖的初始分布的平均值和分布叶子。例如，枫叶开始时往往含有大量糖分，这意味着在类似条件下它们会比其他叶子变红。

右下角的 LEAF-DISPLAY-MODE 改变了树叶的描绘方式。通常，每片叶子都被绘制成一个实心的 NetLogo“默认”海龟楔。当 LEAF-DISPLAY-MODE 设置为默认值“solid”时就是这种情况。为 LEAF-DISPLAY-MODE 选择不同的值，然后根据所选变量更改每个叶子以显示空、半满或全图形。因此，如果 LEAF-DISPLAY-MODE 设置为“水”，每片叶子将显示为空的（如果缺水）、半满的（如果有一些水）或满的（如果相对充满水）。

## 看一看

叶子在被雨滴击中时会吸收水分——这意味着在小雨和很多叶子的情况下，“里面”的叶子将无法获得足够的雨水。

风越大，每片叶子都摇晃得越厉害。

您可以通过将风和雨调到最大值来模拟小型飓风。看着叶子一下子掉光！

## 试一试

你能让所有的叶子在落下之前都变红吗？跌倒前变成橙色怎么办？黄色呢？现在试着把东西混合起来，这样同一棵树就会有许多不同颜色的叶子。 （提示：您可能需要多次调整阳光、雨水和温度滑块，以使组合看起来正确。）

试着让叶子变黄然后再变绿。

在某些气候条件下，树木不会落叶。你能调整气候条件，让树保留一些叶子吗？

## 改一改

在现实生活中，面对太阳的叶子往往比不面对太阳的叶子更黄。更改模型，以便可以定位（和移动）太阳，并且使面向太阳的叶子对阳光比没有的叶子更敏感。

该模型在处理水和糖的方法上非常简单——即所有的水都来自雨水，雨滴可以影响多片叶子，雨滴在撞击地面时会消失。更现实的模型是雨滴一碰到树叶就消失，但也允许水从地面通过树干和树枝流到树叶。还有一个更现实的模型将模拟叶子之间的糖运输和根中多余糖的储存。

除了成熟叶子的死亡之外，还允许新叶子的萌芽和生长。

添加昼夜循环，其中温度下降，太阳下山几个时钟周期。在黑暗中，树会消耗糖分并通过呼吸产生水。

## NetLogo 语言特性

因为 NetLogo 的色彩空间是不完整的，我们需要对颜色混合进行一点伪装。请注意我们如何通过阈值处理设置颜色。这意味着如果天气条件有点极端，可能会出现突然的、不和谐的颜色变化。

另请注意，NetLogo 颜色方案和 `scale-color` 原语会产生从白色到黑色的颜色。因为在这个模型中我们想要改变色调而不是太接近黑色或白色，所以我们使用了 `scale-color` 原语，但在 -50 到 150 的范围内，而不是通常的 0 到 100。

当一片叶子死去时，它的种类就会变成“枯叶”。这将其保留在系统中，但允许我们仅向那些活着的叶子发出指令。

请注意太阳的颜色如何随着 SUN-INTENSITY 的变化而变化。当太阳变暗变小时，标签会发生什么变化？

## 相关模型

Plant Growth is in some ways a model of the opposite process, namely how do leaves grow, as opposed to how do leaves die.

## 引用和致谢

- www.scifun.org/CHEMWEEK/AutumnColors2017.pdf
- https://www.the-scientist.com/news/why-leaves-turn-color-in-the-fall-53929

Thanks to Reuven Lerner for his work on this model.

## 如何引用本模型

If you mention this model or the NetLogo software in a publication, we ask that you include the citations below.

For the model itself:

* Wilensky, U. (2005).  NetLogo Autumn model.  http://ccl.northwestern.edu/netlogo/models/Autumn.  Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

Please cite the NetLogo software as:

* Wilensky, U. (1999). NetLogo. http://ccl.northwestern.edu/netlogo/. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

## 版权和授权

Copyright 2005 Uri Wilensky.

![CC BY-NC-SA 3.0](http://ccl.northwestern.edu/images/creativecommons/byncsa.png)

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License.  To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/3.0/ or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

Commercial licenses are also available. To inquire about commercial licenses, please contact Uri Wilensky at uri@northwestern.edu.

<!-- 2005 -->