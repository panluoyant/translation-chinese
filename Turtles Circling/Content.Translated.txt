﻿*** a9ab0d60c4bb64a6ae5b2b0bde5e0b25
## 这个模型是什么？

这是一种新的数学研究——我们正在研究由许多海龟以简单的方式独立移动所产生的新兴图形。每只海龟都在移动以创建一个固定半径的圆（由 RADIUS滑块设置）。如果他们都在盘旋的半径在中途改变会发生什么？在你尝试之前猜一猜。

## 这个模型是怎么运行的

海龟通过向前移动一点并向右转一点来创建它们的圆圈，以便最终形成一个指定半径的圆圈。我们在该半径的圆上启动所有海龟，以便它们围绕圆移动。

## 如何这个模型怎么用

NUMBER滑块确定盘旋的海龟数量。

RADIUS滑块确定每只海龟移动的圆的大小。

SPEED滑块决定了每只海龟在每个时钟周期中走多远——它决定了盘旋的速度。

SETUP 按钮在以点 (0 0) 为中心的半径为 RADIUS 的圆上创建 NUMBER 个海龟。海龟都朝着周围的圆圈移动。

ALL-CIRCLE 按钮开始海龟盘旋。他们每个人都在画图自己的半径 RADIUS 圆。

在海龟盘旋时更改 RADIUS滑块的值。 *在您这样做之前*，您对更改 RADIUS 时会发生什么的猜测是什么？

## 看一看

海龟所描述的图形发生了什么变化？

海龟多远？

他们进来多远？

## 试一试

尝试不同的起始半径和更改半径值。

您还可以使用以下控件进行进一步调查：

ZERO-CIRCLE 按钮让您只专注于海龟零的运动——所有其余的都停止。

DRAW-CIRCLE 按钮可让您在补丁上绘制一个半径等于 DRAW-RAD 的圆。这样您就可以跟踪海龟的运动。

如果情节？开关打开，该图表将显示海龟零距离原点的距离作为海龟圈的图表。

在海龟盘旋时尝试命令“lt 50”。这与您在更改半径时观察到的行为相同吗？

在命令中心，使用命令 `pen-down` (`pd`) 让单个（或多个）海龟跟踪它们的路径。这可能有助于显示单个海龟的圆圈与您看到的圆圈之间的关系，因为它们都一起移动。

## 改一改

您可以构建哪些工具来帮助可视化正在发生的事情？

## NetLogo 语言特性

`display` 命令用于平滑动画。

## 如何引用本模型

If you mention this model or the NetLogo software in a publication, we ask that you include the citations below.

For the model itself:

* Wilensky, U. (1997).  NetLogo Turtles Circling model.  http://ccl.northwestern.edu/netlogo/models/TurtlesCircling.  Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

Please cite the NetLogo software as:

* Wilensky, U. (1999). NetLogo. http://ccl.northwestern.edu/netlogo/. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

## 版权和授权

Copyright 1997 Uri Wilensky.

![CC BY-NC-SA 3.0](http://ccl.northwestern.edu/images/creativecommons/byncsa.png)

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License.  To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/3.0/ or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

Commercial licenses are also available. To inquire about commercial licenses, please contact Uri Wilensky at uri@northwestern.edu.

This model was created as part of the project: CONNECTED MATHEMATICS: MAKING SENSE OF COMPLEX PHENOMENA THROUGH BUILDING OBJECT-BASED PARALLEL MODELS (OBPML).  The project gratefully acknowledges the support of the National Science Foundation (Applications of Advanced Technologies Program) -- grant numbers RED #9552950 and REC #9632612.

This model was converted to NetLogo as part of the projects: PARTICIPATORY SIMULATIONS: NETWORK-BASED DESIGN FOR SYSTEMS LEARNING IN CLASSROOMS and/or INTEGRATED SIMULATION AND MODELING ENVIRONMENT. The project gratefully acknowledges the support of the National Science Foundation (REPP & ROLE programs) -- grant numbers REC #9814682 and REC-0126227. Converted from StarLogoT to NetLogo, 2001.

<!-- 1997 2001 -->