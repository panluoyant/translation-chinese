﻿*** a9ab0d60c4bb64a6ae5b2b0bde5e0b25
## 这个模型是什么？

该模型模拟了分子生物学中的一个复杂现象：基因的“开关”（打开和关闭）取决于环境条件。通过特定调控蛋白和特定 DNA 序列之间的分子相互作用，每个调控基因响应环境刺激而开启或关闭。

具体来说，它是一种细菌（大肠杆菌）的 [lac-operon](https://en.wikipedia.org/wiki/Lac_operon) 模型，它负责乳糖的摄取和消化，当乳糖是环境中首选的能源。它模拟酶 [permease (LacY)](https://en.wikipedia.org/wiki/Lactose_permease) 和 [β-半乳糖苷酶 (LacZ)](https://en.wikipedia.org/) 合成的调节wiki/β-半乳糖苷酶）。

## 这个模型是怎么运行的

这种基因开关本质上是一个正反馈循环。它负责调节进行乳糖摄取和消化所需的蛋白质（本模型中的 LacZ 和 LacY）的合成。

在这个模型中，实际上有两种糖：葡萄糖和乳糖。与乳糖相比，葡萄糖是“首选”的能源。当周围环境中有葡萄糖和/或没有乳糖时，基因开关处于“关闭稳态”。这是因为阻遏蛋白 [LacI](https://en.wikipedia.org/wiki/Lac_repressor) 通过与 [操作位点](https://en. wikipedia.org/wiki/Operon#General_structure) 的 DNA。在这种稳定状态下，产生的渗透酶 (LacY) 和 β-半乳糖苷酶 (LacZ) 相对较少。

当乳糖被引入外部环境时，乳糖分子通过渗透酶蛋白 (LacYs) 进入细菌细胞。一些进入细胞的乳糖分子与 LacIs 结合，阻止 LacIs 与 DNA 的操纵位点结合。反过来，这会导致产生更多的 LacY。 LacYs 被插入细胞壁，导致更多的乳糖进入细胞，从而形成一个正反馈循环。同时，LacZs 在细胞内消化乳糖分子以产生能量。

当环境中有乳糖而没有葡萄糖时，基因开关就会打开。通过将单元格的颜色更改为蓝色阴影来表示打开开关。

葡萄糖的影响（通过 [cAMP]（https://en.wikipedia.org/wiki/Cyclic_adenosine_monophosphate））仅被隐式建模。当存在葡萄糖时，产生 LacZ 和 LacI 的速率显着降低。

### 重要蛋白质
1. *RNAP* – 这些是从 DNA 合成 mRNA 的 RNA 聚合酶。这些由模型中的棕色斑点表示。该模型不包括 mRNA。
2. *LacI* – 模型中的紫色形状代表阻遏物（LacI 蛋白）。它们与 DNA 的操纵子区域（见下文）结合，不会让 RNAP 沿着基因传递，从而停止蛋白质合成。当乳糖与 LacI 结合时，它们形成 LacI-乳糖复合物（显示为带有灰色圆点的紫色图形）。这些复合物不能与 DNA 的操纵子区域结合。
3. *LacY* - 这些在模型中显示为浅红色矩形。它们是在 RNAP 沿基因传递时产生的。当它们撞击细胞壁时，它们会安装在细胞壁上（由光斑显示）。来自外部环境的乳糖（灰色五边形）通过这些浅红色斑块在细胞内运输。
4. *LacZ* – 这些显示为淡红色的蛋白质。它们存在于细胞内。当它们与乳糖分子碰撞时，乳糖分子被消化并产生能量。

### DNA 区域
该模型中有四个重要的 DNA 区域：

1. *Promoter* – 该区域以绿色表示。当 RNAP 与启动子区域结合时，如果操纵子是自由的，它会沿着 DNA 移动以开始转录。
2. *运营商* - 该区域以橙色表示。阻遏蛋白 LacI 与该区域结合并阻止 RNAP 沿 DNA 移动。
3. *Operon* – 这由蓝色表示。这就是 lacY 和 lacZ 基因所在的位置。该模型仅包括操纵子的这两个基因。
4. *终结者* – 这由灰色表示。当 RNAP 到达该区域时，它会从 DNA 中分离出来。

当 RNAP 沿着基因移动时，会产生 LacY 和 LacZ 蛋白（每个转录五个分子）。在这个模型中，我们没有显示核糖体的翻译。

### 细胞能量
生产和维持细胞的蛋白质机器需要能量。因此，当细胞产生蛋白质并维持这些蛋白质（RNAPs、LacIs 和 LacZs）时，它的能量就会减少。

当细胞内的乳糖被消化时，细胞的能量就会增加。

＃＃＃ 细胞分裂
当细胞的能量从其初始值翻倍时，它分裂成两个子细胞。这些细胞中的每一个都具有原始细胞一半的能量以及原始细胞中每种蛋白质的一半数量。

## 如何这个模型怎么用

＃＃＃ 建立

每个滑块控制这个基因调控电路的某个方面。有关每个变量的功能的更多信息，请参阅滑块部分。

将所有滑块设置为所需级别后，用户应单击 SETUP 以初始化单元格。

＃＃＃ 跑步

要观察基因开关的切换行为，请设置 GLUCOSE?对 ON 和 LACTOSE？关闭。让模拟运行几百个周期。随着时间的推移和细胞分裂，观察细胞能量的变化。当细胞分裂时，能量下降到一半。

要更快地运行模拟，请取消选中几千个周期的“查看更新”框。观察图中的 LacZ 生产。由于细胞中分子相互作用的随机性，它会有所不同。即使开关“关闭”，这种可变性也是遗传开关被称为“泄漏”的原因。

设置葡萄糖？关闭和乳糖？到 ON 以通过观察 LacZ 生产的变化来查看基因开关的行为。

虽然分子相互作用（微观）最好通过运行模拟并选中“视图更新”来观察，但要观察细胞行为（宏观），应该取消选中“视图更新”并且模拟应该运行数千个周期。

＃＃＃ 纽扣

SETUP 按钮初始化模型。

GO 按钮运行模拟，直到细胞死亡。

### 开关

乳糖？ - 如果打开，则将乳糖添加到外部介质（细胞外）

葡萄糖？ - 如果打开，则将葡萄糖添加到外部介质中。葡萄糖是隐式建模的，这意味着当葡萄糖？为 ON，细胞的能量以恒定速率增加（10 个单位/周期）。此外，由于葡萄糖是一种“首选能源”，因此当 GLUCOSE?开启。

### 滑块

LACI-NUMBER - 设置 Laci 的数量

RNAP-NUMBER - 设置 RNAP 的数量

LACI-BOND-LEAKAGE - 这是与操作符结合的 LacI 分子与操作符分离的机会

LACI-LACTOSE-BINDING-CHANCE - 设置乳糖和 LacI 结合形成 LacI-乳糖复合物的机会

LACI-LACTOSE-SEPARATION-CHANCE - 设置 LacI-乳糖复合物分离的机会

LACY-DEGRADATION-CHANCE - 设置安装在细胞壁上的 LacY 降解的机会

LACZ-DEGRADATION-CHANCE - 设置细胞中 LacZ 分子降解的机会

### 地块

能量 - 绘制电池中随时间变化的能量

lacZ Number – 绘制细胞内 LacZ 分子的数量

细胞分裂时间 – 绘制两个细胞分裂事件之间的周期数。它可以用作增长率指标。较短的细胞分裂时间表明较高的生长速率。

## 看一看

注意参与控制基因开关的分子机制的三个部分：
1. 通过 LacY 从外到内摄取乳糖
2. 在没有乳糖的情况下被 LacI 抑制
3. 在乳糖存在下形成 LacI-乳糖复合物并去除抑制

注意添加乳糖时细胞能量的变化（即，当 LACTOSE? 为 ON 时，LACTOSE? 为 OFF 时）。

注意细胞分裂时能量的变化。

注意随着时间的推移平均细胞分裂时间的变化。

注意环境条件的变化（葡萄糖的开/关？和乳糖？对平均细胞分裂时间和 LacZ 数的影响。

## 试一试

尝试使基因开关对环境条件更加敏感。只有当乳糖存在时它才应该“打开”，否则应该“关闭”。您可以通过更改滑块参数来做到这一点。

如果细胞开始吸收乳糖并更快地产生能量，平均细胞分裂时间是否也会减少？

## 改一改

在现实生活中，细胞不是扁平的！看看您是否可以使用 NetLogo 3D 将模型扩展到三个维度。

## NetLogo 语言特性

该模型广泛使用 NetLogo 的 `with` 原语。这是因为，虽然不同的蛋白质以不同的“种类”为代表，但蛋白质也以不同的状态存在。

## 相关模型

* GenEvo Curricular Models
* GenDrift Sample Models

## 如何引用本模型

If you mention this model or the NetLogo software in a publication, we ask that you include the citations below.

For the model itself:

* Dabholkar, S., Bain, C. and Wilensky, U. (2016).  NetLogo GenEvo 1 Genetic Switch model.  http://ccl.northwestern.edu/netlogo/models/GenEvo1GeneticSwitch.  Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

Please cite the NetLogo software as:

* Wilensky, U. (1999). NetLogo. http://ccl.northwestern.edu/netlogo/. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

To cite the GenEvo Systems Biology curriculum as a whole, please use:

* Dabholkar, S. & Wilensky, U. (2016). GenEvo Systems Biology curriculum. http://ccl.northwestern.edu/curriculum/genevo/. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

## 版权和授权

Copyright 2016 Uri Wilensky.

![CC BY-NC-SA 3.0](http://ccl.northwestern.edu/images/creativecommons/byncsa.png)

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License.  To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/3.0/ or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

Commercial licenses are also available. To inquire about commercial licenses, please contact Uri Wilensky at uri@northwestern.edu.

<!-- 2016 GenEvo Cite: Dabholkar, S., Bain, C. -->