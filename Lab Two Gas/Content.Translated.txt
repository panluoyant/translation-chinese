﻿*** a9ab0d60c4bb64a6ae5b2b0bde5e0b25
## 这个模型是什么？

该模型模拟了带有分隔壁的盒子中两种不同类型的气体粒子的行为。可以去除部分或全部壁，使颗粒混合在一起。它是最初的 CM StarLogo 应用程序之一（名称为 GPCEE），现在作为互联数学“理解复杂现象”建模项目的一部分被移植到 NetLogo。

该模型是一系列 GasLab 模型中的一个。他们使用相同的基本规则来模拟气体的行为。每个模型都集成了不同的特征，以突出气体行为的不同方面。

模型的基本原理是假设气体粒子具有两种基本作用：它们移动并碰撞——与其他粒子或与墙壁等任何其他物体发生碰撞。

该模型是互联数学“理解复杂现象”建模项目的一部分。

## 这个模型是怎么运行的

粒子被建模为没有内部能量的硬球，除了它们的运动。粒子之间的碰撞是弹性的。粒子根据速度着色 --- 蓝色表示慢速，绿色表示中速，红色表示高速。

颗粒的着色与一种速度 (10) 有关。速度小于 5 的粒子是蓝色的，大于 15 的粒子是红色的，而介于两者之间的都是绿色的。

两个粒子碰撞的确切方式如下：
1. 一个粒子在不改变其速度的情况下沿直线运动，除非它与另一个粒子碰撞或从墙上反弹。
2. 如果两个粒子发现自己在同一个格子上，它们就会“碰撞”。
3. 选择一个随机轴，就好像它们是两个互相撞击的球，这个轴是连接它们中心的线。
4. 根据动量和能量守恒，它们沿该轴交换动量和能量。该计算是在质心系统中完成的。
5. 每个粒子都被赋予了新的速度、能量和方向。
6. 如果一个粒子发现自己在容器壁上或非常靠近容器壁，它会“反弹”——即反射它的方向并保持相同的速度。

## 如何这个模型怎么用

初始设置：
- BOX-SIZE：盒子将占据的世界宽度和高度的百分比。
- NUM-MAGENTAS 和 NUM-CYAN：每种颜色的气体粒子数。
- Magenta-INIT-SPEED 和 CYAN-INIT-SPEED：各自粒子的初始速度。
- Magenta-MASS 和 CYAN-MASS：各自粒子的质量。

SETUP 按钮将设置这些初始条件。
GO 按钮将开始模拟。

控制：
OPEN 按钮打开盒子腔室之间的墙壁。
CLOSE 按钮关闭盒子腔室之间的墙壁。

其他设置：
- COLLIDE?：打开和关闭粒子之间的碰撞。
- OPENING-SIZE：按下 OPEN 按钮时以 BOX-SIZE 百分比表示的开口尺寸。

监视器：
- 右室中的洋红色：右室中的洋红色粒子数。
- 左室中的青色：左室中的青色颗粒数量
- 平均能量洋红色或青色：洋红色或青色粒子的平均能量。
- 平均速度洋红色或青色：洋红色或青色粒子的平均速度。

情节：
- 平均能量：不同粒子随时间的平均能量。
- 平均速度：不同粒子随时间的平均速度。

## 看一看

当墙壁被移除时，哪些变量会影响模型达到新平衡的速度？

为什么即使平均能量保持不变，当模型在墙壁就位的情况下运行时，每种颜色的平均速度都会降低？

当它们混合时，每种粒子的相对能量和速度会发生什么变化？初始速度和质量对这种关系有什么影响？

系统是否达到平衡状态？

当能量分布达到平衡时，较重的粒子往往具有更高或更低的速度？

这个盒子可以被认为是“绝缘的”是否合理？

## 试一试

计算模型需要多长时间才能在不同大小的窗口下达到平衡（保持其他参数不变）。

计算模型在不同粒子速度下达到平衡需要多长时间。

将青色粒子的数量设置为零。这是气体膨胀到真空中的模型。这个实验首先由 Joule 完成，使用两个由阀门隔开的绝缘室。他发现当阀门打开时，气体的温度保持不变。为什么这是真的？这个模型与那个观察一致吗？

尝试一些极端情况，来测试你的直觉理解：
- 质量相同，两个粒子的速度非常不同。
——速度相同，质量大不相同。
——极少数的一种粒子——几乎，但不完全是真空。那少数粒子会发生什么，它们如何影响另一种粒子？

尝试将壁打开后两种气体的平衡速度之比与两种气体的质量之比定量联系起来。它们有什么关系？

## 改一改

监测左右室的压力。

监测左右室的温度。

用可移动的活塞代替隔壁，使两种颗粒相互推挤，不混杂。那么他们会达到不同的平衡吗？

用可以传递能量的表面代替隔墙。

添加能量和速度分布的直方图（例如在“自由气体”模型中找到的）。

## NetLogo 语言特性

请注意海龟如何检测碰撞以及代码如何保证相同的两个粒子不会碰撞两次。如果我们让补丁检测到它们会发生什么？

## 引用和致谢

This model was developed as part of the GasLab curriculum (http://ccl.northwestern.edu/curriculum/gaslab/) and has also been incorporated into the Connected Chemistry curriculum (http://ccl.northwestern.edu/curriculum/ConnectedChemistry/)

## 如何引用本模型

If you mention this model or the NetLogo software in a publication, we ask that you include the citations below.

For the model itself:

* Wilensky, U. (1997).  NetLogo GasLab Two Gas model.  http://ccl.northwestern.edu/netlogo/models/GasLabTwoGas.  Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

Please cite the NetLogo software as:

* Wilensky, U. (1999). NetLogo. http://ccl.northwestern.edu/netlogo/. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

## 版权和授权

Copyright 1997 Uri Wilensky.

![CC BY-NC-SA 3.0](http://ccl.northwestern.edu/images/creativecommons/byncsa.png)

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License.  To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/3.0/ or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

Commercial licenses are also available. To inquire about commercial licenses, please contact Uri Wilensky at uri@northwestern.edu.

This model was created as part of the project: CONNECTED MATHEMATICS: MAKING SENSE OF COMPLEX PHENOMENA THROUGH BUILDING OBJECT-BASED PARALLEL MODELS (OBPML).  The project gratefully acknowledges the support of the National Science Foundation (Applications of Advanced Technologies Program) -- grant numbers RED #9552950 and REC #9632612.

This model was converted to NetLogo as part of the projects: PARTICIPATORY SIMULATIONS: NETWORK-BASED DESIGN FOR SYSTEMS LEARNING IN CLASSROOMS and/or INTEGRATED SIMULATION AND MODELING ENVIRONMENT. The project gratefully acknowledges the support of the National Science Foundation (REPP & ROLE programs) -- grant numbers REC #9814682 and REC-0126227. Converted from StarLogoT to NetLogo, 2002.

<!-- 1997 2002 -->