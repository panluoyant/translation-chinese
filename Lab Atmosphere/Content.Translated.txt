﻿*** a9ab0d60c4bb64a6ae5b2b0bde5e0b25
## 这个模型是什么？

在这个模型中，气态大气被置于“行星”表面之上，由世界底部的黄线表示。

该模型是一系列 GasLab 模型中的一个。他们使用相同的基本规则来模拟气体的行为。每个模型都集成了不同的特征，以突出气体行为的不同方面。

模型的基本原理是假设气体粒子具有两种基本作用：它们移动并碰撞——与其他粒子或与墙壁等任何其他物体发生碰撞。

## 这个模型是怎么运行的

所有 GasLab 模型的基本原理是以下算法（更多详细信息，请参见模型“GasLab Gas in a Box”）：

1）一个粒子在不改变其速度的情况下沿直线运动，除非它与另一个粒子碰撞或从墙上反弹。
2) 如果两个粒子发现自己在同一个格子上，它们就会“碰撞”（NetLogo 的视图由称为补丁的小方块网格组成）。在这个模型中，两个粒子的目标是使它们在原点发生碰撞。
3) 选择粒子的碰撞角度，就好像它们是两个撞击的实心球一样，这个角度描述了连接它们中心的线的方向。
4）粒子只沿着这条线交换动量和能量，符合弹性碰撞的动量和能量守恒。
5) 每个粒子都被赋予了新的速度、方向和能量。
6）如果一个粒子发现自己位于或非常接近行星表面，它会“反弹”——即反射它的方向并保持相同的速度。

在这个模型中，重力的影响计算如下：在每个时钟周期期间，每个粒子都被赋予了额外的向下速度，就像它在重力场中一样。粒子从“地面”反弹。如果他们到达世界之巅，他们就会消失，就好像他们逃离了地球的引力场一样。丢失粒子的百分比显示在 PERCENT LOST PARTTICLES 监视器中。

## 如何这个模型怎么用

初始设置：
- NUMBER-OF-PARTICLES：气体粒子的数量
- INIT-PARTICLE-SPEED：每个粒子的初始速度
- 粒子质量：每个粒子的质量
- GRAVITY-ACCELERATION：重力加速度的值

SETUP 按钮将设置初始条件。 GO 按钮将运行模拟。

其他设置：
- COLLIDE?：打开和关闭粒子之间的碰撞。
- TRACE?：跟踪其中一个粒子的路径。

监视器：
- 平均速度：粒子的平均速度。
- 快速百分比、中百分比、慢百分比：不同速度的粒子百分比 - 快（红色）、中（绿色）和慢（蓝色）。
- 丢失的粒子百分比：在世界顶部边缘消失的粒子百分比。
- CLOCK（在视图内）：已运行的周期数。

情节：
- 速度计数：图表每个速度范围内的粒子数。
- 速度直方图：所有粒子的速度分布。灰线是平均值，黑线是初始平均值。
- 能量直方图：所有粒子的能量分布，使用 m*(v^2)/2。
- 密度直方图：显示大气每个“层”的粒子数量，即其局部密度。
- 能源与。高度：显示大气每一“层”的粒子的平均能量。

## 看一看

尝试预测一段时间后视图会是什么样子，以及为什么。

观察一个粒子的灰色路径。你对它的运动有什么看法？转向碰撞？关闭，看看是否有任何差异。

观察模型运行时密度分布的变化。

当模型运行时，粒子的平均速度和动能会发生什么变化？如果他们获得能量，它来自哪里？速度和能量分布会发生什么变化？

## 试一试

当重力增加或减少时会发生什么？

更改初始数量、速度和质量。密度分布会发生什么变化？

哪些因素会影响有多少粒子逃离这个星球？

这个模型会达到某种平衡吗？你怎么知道它什么时候到达？

试着找出这个模型中粒子的分布是否与传统物理定律所预测的相同。例如，这是否与高海拔地区气压较低（因此空气密度较低）的事实一致？为什么他们更冷？

尝试使重力为负。

## 改一改

找到一种方法来图表气体的相对“温度”作为与行星距离的函数。

用不同质量的粒子试试这个模型。您可以对每个质量进行不同的着色，以便能够看到它们的去向。它们的分布不同吗？哪些人最容易逃脱？这对大气的组成有何暗示？

粒子在达到一定高度时逃逸的事实并不完全现实，尤其是在粒子即将返回地球的情况下。通过允许“逃逸”的粒子在重力将它们拉回大气层后重新进入大气层来改进模型。这如何改变模型的行为？跟踪实际损失（达到行星逃逸速度的粒子）会很有趣。在什么条件下粒子会达到逃逸速度？

将“行星”变成一个中心点，而不是一个平面。

这个基本模型可用于探索自由移动的粒子对它们施加力的其他情况——例如离心机或电场中的带电粒子（离子）。

## NetLogo 语言特性

由于重力的影响，粒子跟随弯曲的路径。由于 NetLogo 以离散步骤模拟时间，因此这些曲线路径必须用一系列短直线来近似。这是轻微不准确的根源，如果模型运行很长时间，粒子会逐渐失去能量。效果就好像与地面的碰撞略微缺乏弹性。可以通过增加 `vsplit` 来减少不准确性，但模型会运行得更慢。

## 相关模型

This model is part of the GasLab suite and curriculum.
See, in particular, the models "Gas in a Box" and "Gravity Box", which is a modified version of the "Atmosphere" model, with a ceiling on the atmosphere.

## 引用和致谢

This model was developed as part of the GasLab curriculum (http://ccl.northwestern.edu/curriculum/gaslab/) and has also been incorporated into the Connected Chemistry curriculum (http://ccl.northwestern.edu/curriculum/ConnectedChemistry/)

## 如何引用本模型

If you mention this model or the NetLogo software in a publication, we ask that you include the citations below.

For the model itself:

* Wilensky, U. (1997).  NetLogo GasLab Atmosphere model.  http://ccl.northwestern.edu/netlogo/models/GasLabAtmosphere.  Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

Please cite the NetLogo software as:

* Wilensky, U. (1999). NetLogo. http://ccl.northwestern.edu/netlogo/. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

## 版权和授权

Copyright 1997 Uri Wilensky.

![CC BY-NC-SA 3.0](http://ccl.northwestern.edu/images/creativecommons/byncsa.png)

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License.  To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/3.0/ or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

Commercial licenses are also available. To inquire about commercial licenses, please contact Uri Wilensky at uri@northwestern.edu.

This model was created as part of the project: CONNECTED MATHEMATICS: MAKING SENSE OF COMPLEX PHENOMENA THROUGH BUILDING OBJECT-BASED PARALLEL MODELS (OBPML).  The project gratefully acknowledges the support of the National Science Foundation (Applications of Advanced Technologies Program) -- grant numbers RED #9552950 and REC #9632612.

This model was converted to NetLogo as part of the projects: PARTICIPATORY SIMULATIONS: NETWORK-BASED DESIGN FOR SYSTEMS LEARNING IN CLASSROOMS and/or INTEGRATED SIMULATION AND MODELING ENVIRONMENT. The project gratefully acknowledges the support of the National Science Foundation (REPP & ROLE programs) -- grant numbers REC #9814682 and REC-0126227. Converted from StarLogoT to NetLogo, 2002.

<!-- 1997 2002 -->